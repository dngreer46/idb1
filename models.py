# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:postgres@localhost:5432/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Book(db.Model):
	__tablename__ = 'book'

	title = db.Column(db.String(80), nullable = False)
	id = db.Column(db.Integer, primary_key = True, nullable = False)
	isbn = db.Column(db.String(50))
	author = db.Column(db.String(50))
	publication_date = db.Column(db.String(20))
	google_id = db.Column(db.String(20))
	image = db.Column(db.String(200))
	description = db.Column(db.String)
	publisher = db.Column(db.String)


class Author(db.Model):
	__tablename__ = 'author'

	name = db.Column(db.String(50), nullable = False)
	id = db.Column(db.Integer, primary_key = True, nullable = False)
	image = db.Column(db.String)
	born = db.Column(db.String)
	nationality = db.Column(db.String)
	education = db.Column(db.String)
	almaMater = db.Column(db.String)
	description = db.Column(db.String)
	wiki = db.Column(db.String)
	title = db.Column(db.String)
	publisher = db.Column(db.String)

class Publisher(db.Model):
	__tablename__ = 'publisher'

	name = db.Column(db.String(50))
	id = db.Column(db.Integer, primary_key = True, nullable = False)
	image = db.Column(db.String(200))
	website = db.Column(db.String(50))
	parent = db.Column(db.String(50))
	owner = db.Column(db.String(50))
	founded = db.Column(db.String(50))
	location = db.Column(db.String(200))
	description = db.Column(db.String)
	wiki = db.Column(db.String(200))
	author = db.Column(db.String)
	title = db.Column(db.String)


db.drop_all()
db.create_all()
# End of models.py
