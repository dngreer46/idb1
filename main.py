#-----------------------------------------
# main2.py
# creating first flask application
#-----------------------------------------
import subprocess
from flask import Flask, render_template
from models import app, db, Book, Author, Publisher
from create_db import create_books, create_authors, create_publishers


@app.route('/')
def index():
 return render_template('index.html')

@app.route('/about/')
def About():
 return render_template('About.html')

@app.route('/test/')
def test():
    return render_template('test.html')

@app.route('/authors/')
def Authors():
    authors = db.session.query(Author).all()
    return render_template('Authors2.html', authors = authors)

@app.route('/books/')
def Books():
    books = db.session.query(Book).all()
    return render_template('Books2.html', books = books)

@app.route('/publishers/')
def Publishers():
    publishers = db.session.query(Publisher).all()
    return render_template('Publishers2.html', publishers = publishers)

@app.route('/author/<author_name>')
def author_page(author_name):
    pub = db.session.query(Publisher).all()
    author = db.session.query(Author).all()
    books = db.session.query(Book).all()

    for i in author:
        if i.name == author_name:
            born = i.born
            description = i.description
            education = i.education
            nationality = i.nationality
            almaMater = i.almaMater
            wiki = i.wiki
            image = i.image

    list_pubs = []
    list_books = []

    for i in pub:
        if i.author == author_name:
            if i.name not in list_pubs:
                list_pubs.append(i.name)

    for i in books:
        for x in author:
            if x.name == author_name:
                if x.title == i.title:
                    if i.title not in list_books:
                        list_books.append(i.title)

    for i in books:
        if i.author == author_name:
            if i.title not in list_books:
                list_books.append(i.title)

    return render_template('author_template.html', pub = pub, author_name = author_name, born = born, description = description, education = education, nationality = nationality, almaMater = almaMater, wiki = wiki, image = image, publishers = list_pubs, titles = list_books)


@app.route('/book/<book_title>')
def book_page(book_title):
    pub = db.session.query(Publisher).all()
    author = db.session.query(Author).all()
    books = db.session.query(Book).all()

    list_a = []
    for i in books:
        if i.title == book_title:
            for x in author:
                if x.title == book_title:
                    list_a.append(x.name)
            if i.author not in list_a:
                list_a.append(i.author)

            google_id = i.google_id
            isbn = i.isbn
            image = i.image
            publication_date = i.publication_date
            description = i.description
            publisher = i.publisher

    return render_template('book_template.html', pub = pub, book_title = book_title, google_id = google_id, isbn = isbn, image = image, publication_date = publication_date, description = description, publisher = publisher, author = list_a)

@app.route('/publisher/<publisher_name>')
def publisher_page(publisher_name):
    pub = db.session.query(Publisher).all()
    author = db.session.query(Author).all()
    books = db.session.query(Book).all()

    for i in pub:
        if i.name == publisher_name:
            owner = i.owner
            description = i.description
            website = i.website
            image = i.image
            founded = i.founded
            location = i.location
            wiki = i.wiki

    list_a = []
    for i in author:
        if i.publisher == publisher_name:
            if i.name not in list_a:
                list_a.append(i.name)

    for i in books:
        if i.publisher == publisher_name:
            if i.author not in list_a:
                list_a.append(i.author)

    list_b = []
    for i in books:
        if i.publisher == publisher_name:
            if i.title not in list_b:
                list_b.append(i.title)

    return render_template('publisher_template.html', pub = pub, publisher_name = publisher_name, owner = owner, description = description, website = website, image = image, authors = list_a, titles = list_b, location = location, founded = founded)


if __name__ == "__main__":
 app.run()
#----------------------------------------
# end of main2.py
#-----------------------------------------
