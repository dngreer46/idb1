import os
import sys
import unittest
from models import db, Book, Author, Publisher

class DBTestCases(unittest.TestCase):

    #Book Test 2, Search by title to get isbn
    def test_book_insert_2(self):
        a = Book(id = '123654', isbn='9780547577517', title = 'The Things They Carried')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Book).filter_by(title='The Things They Carried').one()
        self.assertEqual(str(r.isbn), '9780547577517')

        db.session.delete(a)
        db.session.commit()

    #Book Test 3, Search by id to get title
    def test_book_insert_3(self):
        a = Book(id = '123987', isbn='1429914564', title = 'The House on Mango Street')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Book).filter_by(id='123987').one()
        self.assertEqual(str(r.title), 'The House on Mango Street')

        db.session.delete(a)
        db.session.commit()

    #Author Test 1, Search by name
    def test_author_insert_1(self):
        a = Author(id = '456987', name = 'Sandra Cisneros')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Author).filter_by(name = 'Sandra Cisneros').one()
        self.assertEqual(str(r.name), 'Sandra Cisneros')

        db.session.delete(a)
        db.session.commit()

    #Author Test 2, Search by name to get title
    def test_author_insert_2(self):
        a = Author(id = '456321', name = 'Sandra Cisneros', title = 'The House on Mango Street')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Author).filter_by(name = 'Sandra Cisneros').one()
        self.assertEqual(str(r.title), 'The House on Mango Street')

        db.session.delete(a)
        db.session.commit()

    #Author Test 3, Search by almaMater from name
    def test_author_insert_3(self):
        a = Author(id = '789321', name = 'Tim O\'Brien', almaMater = 'Harvard Uni')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Author).filter_by(name = 'Tim O\'Brien').one()
        self.assertEqual(str(r.almaMater), 'Harvard Uni')

        db.session.delete(a)
        db.session.commit()

    #Publisher Test 1, Search by publisher name
    def test_publisher_insert_1(self):
        a = Publisher(id = '159357', name = 'Penguin Random Stuff')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Publisher).filter_by(name = 'Penguin Random Stuff').one()
        self.assertEqual(str(r.name), 'Penguin Random Stuff')

        db.session.delete(a)
        db.session.commit()

    #Publisher Test 2, Search by name to get book title
    def test_publisher_insert_2(self):
        a = Publisher(id = '753951', name = 'Penguin Random Lounge', title = 'The Hunts for Red October')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Publisher).filter_by(name = 'Penguin Random Lounge').one()
        self.assertEqual(str(r.title), 'The Hunts for Red October')

        db.session.delete(a)
        db.session.commit()

    #Publisher Test 3, Search by location to get wiki
    def test_publisher_insert_3(self):
        a = Publisher(id = '951357', name = 'Penguin Random Safe', location = 'New York New York', wiki = 'http://en.wikipedia.org/wiki/Penguin_Randoms_House')
        db.session.add(a)
        db.session.commit()

        r = db.session.query(Publisher).filter_by(location = 'New York New York').one()
        self.assertEqual(str(r.wiki), 'http://en.wikipedia.org/wiki/Penguin_Randoms_House')

        db.session.delete(a)
        db.session.commit()

if __name__ == '__main__':
    unittest.main()
