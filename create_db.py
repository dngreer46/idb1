# beginning of create_db.py
import json
from models import db, Book, Author, Publisher

def load_json(filename):
	with open(filename) as file:
		jsn = json.load(file)
		file.close()

	return jsn

def create_books():
	book = load_json('books.json')

	id_count = 0

	for oneBook in book['Books']:
		title = oneBook['title']
		if 'isbn' in oneBook:
			isbn = oneBook['isbn']
		if 'publication_date' in oneBook:
			publication_date = oneBook['publication_date']
		google_id = oneBook['google_id']
		name = oneBook['authors'][0]['name']
		image = oneBook['image_url']
		if 'description' in oneBook:
			description = oneBook['description']
		publisher = oneBook['publishers'][0]['name']

		newBook = Book(description = description, image = image, author = name, google_id = google_id, publication_date = publication_date,
			isbn = isbn, title = title, id = id_count, publisher = publisher)

		# After I create the book, I can then add it to my session.
		db.session.add(newBook)
		# commit the session to my DB.
		db.session.commit()
		id_count += 1

def create_authors():
	book = load_json('books.json')
	id_count = 0

	for oneBook in book['Books']:
		title = oneBook['title']
		oneAuthor = oneBook['authors'][0]
		name = oneAuthor['name']
		publisher = oneBook['publishers'][0]['name']
		if 'born' in oneAuthor:
			born = oneAuthor['born']
		if 'education' in oneAuthor:
			education = oneAuthor['education']
		else:
			education = "N/A"
		if 'nationality' in oneAuthor:
			nationality = oneAuthor['nationality']
		else:
			nationality = "N/A"
		if 'description' in oneAuthor:
			description = oneAuthor['description']
		else:
			description = "N/A"
		if 'alma_mater' in oneAuthor:
			alma_mater = oneAuthor['alma_mater']
		else:
			alma_mater = "N/A"
		if 'wikipedia_url' in oneAuthor:
			wiki = oneAuthor['wikipedia_url']
		else:
			wiki = "N/A"
		if 'image_url' in oneAuthor:
			image = oneAuthor['image_url']
		else:
			image = ""

		newAuthor = Author(born = born, name = name, education = education, nationality = nationality, description = description,
			almaMater = alma_mater, wiki = wiki, image = image, id = id_count, title = title, publisher = publisher)

		if db.session.query(Author).filter(Author.name==newAuthor.name).count() == 0:
			db.session.add(newAuthor)
			db.session.commit()
			id_count += 1


def create_publishers():
	book = load_json('books.json')
	id_count = 0

	for oneBook in book['Books']:
		onePublisher = oneBook['publishers'][0]
		name = onePublisher['name']
		authors = oneBook['authors']
		title = oneBook['title']

		for i in authors:
			author = i['name']

		if 'location' in onePublisher:
			location = onePublisher['location']
		else:
			location = "N/A"
		if 'founded' in onePublisher:
			founded = onePublisher['founded']
		else:
			founded = "N/A"
		if 'wikipedia_url' in onePublisher:
			wiki = onePublisher['wikipedia_url']
		else:
			wiki = "N/A"
		if 'description' in onePublisher:
			description = onePublisher['description']
		else:
			description = "N/A"
		if 'owner' in onePublisher:
			owner = onePublisher['owner']
		else:
			owner = "N/A"
		if 'image_url' in onePublisher:
			image = onePublisher['image_url']
		else:
			image = ""
		if 'website' in onePublisher:
			website = onePublisher['website']
		else:
			website = "N/A"
		if 'parent company' in onePublisher:
			parent = onePublisher['parent company']
		else:
			parent = "N/A"

		newPublisher = Publisher(name = name, wiki = wiki, description = description, owner = owner, parent = parent,
			image = image, website = website, id = id_count, author = author, title = title, founded = founded, location = location)

		if db.session.query(Publisher.id).filter(Publisher.name==newPublisher.name).count() == 0:
			db.session.add(newPublisher)
			db.session.commit()
			id_count += 1


create_books()
create_authors()
create_publishers()
# end of create_db.py
